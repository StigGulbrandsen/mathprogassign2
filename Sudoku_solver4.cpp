#include <iostream>
#include <gecode/int.hh>
#include <gecode/minimodel.hh>
#include <gecode/search.hh>
#include <fstream>
#include <random>

using namespace Gecode;
// Sudoku
int n[81];

class Sudoku : public Space
{
protected:
	IntVarArray l;
public:
	
	Sudoku(void) : l(*this, 81, 1, 9)
	{
		
		for (int i = 0; i < 81; i++)
		{
			if (n[i]) // if n[i] != 0
			{
				// makes a sudoku from n[]
				rel(*this, l[i] == n[i]);
			}
		}

		for ( int k = 0; k < 81; k+=9)
		{
			for (int y = 0; y < 9; y++)
			{
				// Horizontal constraints - unique elements in a row
				// 0-1, 0-2, 0-3
				for (int x = y + 1; x < 9; x++)
				{
					rel(*this, l[y + k] != l[x + k]);
				}
				// Vertical constraints - unique elements in a column
				for (int z = 9 + k; z < 81; z += 9)
				{
					rel(*this, l[k + y] != l[z +y]);
				}
			}
			
		}
		// constraints for 9 3x3 sudokus
		int nextRow = 0;
		for (int e = 0; e < 9; e++)
		{
			if (e % 3 == 0 && e) 
				nextRow += 18;

			int smallNextRow = 0;
			for (int y = 0; y < 9; y++)
			{
				if (y % 3 == 0 && y)  smallNextRow += 6;

				int ekstra = 0;
				for (int x = y + 1; x < 9; x++)
				{
					if (x % 3 == 0 && x)  ekstra += 6;

					rel(*this, l[y + smallNextRow + e * 3 + nextRow] != l[x + ekstra + smallNextRow + e * 3 + nextRow]);
				}

			}
		}


		branch(*this, l, INT_VAR_SIZE_MIN(), INT_VAL_MIN());
	}

	Sudoku(Sudoku& s) : Space(s)
	{
		l.update(*this, s.l);
	}

	Space* copy()
	{
		return new Sudoku(*this);
	}

	void print(void) const
	{
		
		for (int i = 0; i < 81; i++)
		{
			
			if ((i % 9) == 0)
			{
				std::cout << "\n";
			}
			else if ((i % 3) == 0)
			{
				std::cout << " ";
			}
			
			std::cout << l[i] << " ";

		}
		std::cout << "\n";
	}

};
void lesfrafil() {
	std::ifstream innfil("Suk002.txt");

	n[81];
	if (innfil) {
		while (!innfil.eof()) {

			for (int a = 0; a < 81; a++) {
				innfil >> n[a];

			}
			innfil.close();
		}
	}
	else
		std::cout << "IngenFil";
}


int main(int argc, char* argv[])
{

	try
	{
		std::random_device rd;
		std::default_random_engine generator(rd());
		std::uniform_int_distribution<int> distribution(0, 81);

		int count = 0; // number of solutions
		lesfrafil(); // read from file
		while (count < 2) // check if number of solution is more than 2
		{
			count = 0; // reset counter

			n[distribution(generator)] = 0; // set random vari in sudoku to 0

			Sudoku* m = new Sudoku; // makes new sudoku

			std::cout << "The contents of the space before search begins" << std::endl;
			m->print();

			DFS<Sudoku> e(m); 
			delete m;

			while (Sudoku* s = e.next()) // finds all solutions
			{
				s->print(); delete s;
				count++;
			}
		}

		std::cout << "Number of elements: " << count << std::endl;
	}

	catch (Exception e)
	{
		std::cerr << "Gecode exeption: " << e.what() << std::endl;
		return 1;
	}

	return 0;
}

