

#include <SFML/Graphics.hpp>
#include <cmath>
#include <time.h>
#include <iostream>
#include <SFML/Window/Mouse.hpp>
#include <random>

using namespace sf;    //use this so i don't have to write sf every time i want to do a sfml function

std::random_device rd;
std::default_random_engine generator(rd());


float eco; //coordinates for the Earth
float mco; //coordinates for the Moon
float marsco;
float eSwitcher = -0.03; //using this to dictate which direction the EARTH is orbiting
float mSwitcher = 0.04; //using this to dictate which direction the EARTH is orbiting
float maSwitcher = 0.01;

int randx = 0;
int randy = 0; 
float randspde, randspdm, randspdma;
Event event;
int windowx = 1000;
int windowy = 1000;
std::uniform_int_distribution<int> randox(50,windowx-50); 
std::uniform_int_distribution<int> randoy(50,windowy-50); 
std::uniform_real_distribution<float> randos(0.01, 0.06);

Clock Orb;
CircleShape mars(40.f);
CircleShape earth(20.f);
CircleShape moon(10.f);
RenderWindow window(VideoMode(windowx, windowy), "Solar System and other goods");



void randpos(){
    //random velocity and position
    randx = randox(generator);
    randy = randoy(generator);
    randspde = randos(generator);
    randspdm = randos(generator);
    randspdma = randos(generator);
}


//various details about the shapes
void draw(){
    mars.setOrigin(40, 40);    
	earth.setOrigin(20, 20);
	moon.setOrigin(10, 10);

    //velocity for the solar system
    eco += eSwitcher;
	mco += mSwitcher;
    marsco += maSwitcher;

    //velocity for flat motion
    /*
    eco += randspde;
	mco -= randspdm;
    marsco += randspdma;
    */

	//basic stats for the various planets
	mars.setFillColor(Color::Red);
	earth.setFillColor(Color::Blue);
	moon.setFillColor(Color::White);

    //flat motion restricted to the window
    /*
    moon.setPosition(randx,mco +randy);
    earth.setPosition(eco+randx,eco +randy);
    mars.setPosition(marsco + randx,randy);
*/
    //circular motion
	mars.setPosition(300 * cos(marsco * 3.14 / 180.0f) + float(windowx)/2, 300* sin(marsco * 3.14 / 180.0f) + float(windowy)/2);
	earth.setPosition(150 * cos(eco * 3.14 / 180.0f) + mars.getPosition().x, 150 * sin(eco * 3.14 / 180.0f) + mars.getPosition().y);
	moon.setPosition(50 * cos(mco * 3.14 / 180.0f) + earth.getPosition().x, 50 * sin(mco * 3.14 / 180.0f) + earth.getPosition().y);
/*
    //eliptic motion
    mars.setPosition(400 * cos(marsco * 3.14 / 180.0f) + float(windowx)/2, 200* sin(marsco * 3.14 / 180.0f) + float(windowy)/2);
	earth.setPosition(100 * cos(eco * 3.14 / 180.0f) + mars.getPosition().x, 400 * sin(eco * 3.14 / 180.0f) + mars.getPosition().y);
	moon.setPosition(100 * cos(mco * 3.14 / 180.0f) + earth.getPosition().x, 50 * sin(mco * 3.14 / 180.0f) + earth.getPosition().y);
    */
}

int main()
{
    randpos();

	while (window.isOpen()){

    //position for boundaries
    if (moon.getPosition().y+10 >= windowy || moon.getPosition().y-10 <= 0)
        randspdm *= -1;       
    if (mars.getPosition().y+40 >= windowy || mars.getPosition().y-40 <= 0)
        randspdma *= -1;    
    if (earth.getPosition().y+20 >= windowy || earth.getPosition().y-20 <= 0)
        randspde *= -1;        

    if (moon.getPosition().x+10 >= windowx || moon.getPosition().x-10 <= 0)
        randspdm *= -1;       
    if (mars.getPosition().x+40 >= windowx || mars.getPosition().x-40 <= 0)
        randspdma *= -1;    
    if (earth.getPosition().x+20 >= windowx || earth.getPosition().x-20 <= 0)
        randspde *= -1;            

		while (window.pollEvent(event))
		{
			switch (event.type)
			{
			case Event::Closed:

				window.close();

				break;

			case Event::MouseButtonPressed:

				switch (event.key.code)
				{
				case Mouse::Left:
					//switches moon, mars and earth orbit
					if (Mouse::getPosition(window).x < earth.getPosition().x + 20
						&& Mouse::getPosition(window).x > earth.getPosition().x - 20
						&& Mouse::getPosition(window).y < earth.getPosition().y + 20
						&& Mouse::getPosition(window).y > earth.getPosition().y - 20)
						eSwitcher *= -1;
					if (Mouse::getPosition(window).x < moon.getPosition().x + 10
						&& Mouse::getPosition(window).x > moon.getPosition().x - 10
						&& Mouse::getPosition(window).y < moon.getPosition().y + 10
						&& Mouse::getPosition(window).y > moon.getPosition().y - 10)
						mSwitcher *= -1;
					break;
                    if (Mouse::getPosition(window).x < mars.getPosition().x + 40
						&& Mouse::getPosition(window).x > mars.getPosition().x - 40
						&& Mouse::getPosition(window).y < mars.getPosition().y + 40
						&& Mouse::getPosition(window).y > mars.getPosition().y - 40)
						maSwitcher *= -1;
					break;
				}

			}

		}

		draw();                           //draws the stuff
		window.clear();
		window.draw(mars);
		window.draw(earth);
		window.draw(moon);
		window.display();
	}

	return 0;
}